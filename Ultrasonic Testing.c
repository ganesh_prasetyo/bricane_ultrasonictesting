#define TrigPin
#define EchoPin
#define PC4 
#define PC6
#define PC7

const int ledChannel = 0; // select channel 0
const int resolution = 8; // 8 bit resolutin i.e., 0-255
const int frequency = 40000; // set frequency in Hz


void setup (){
  // Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

//Testing PWM with LED  
ledcSetup(ledChannel, frequency, resolution); // configure LED PWM functionalitites
ledcAttachPin(LED_BUILTIN, ledChannel); // attach the channel to the GPIO to be controlled

  // Begin Serial communication at a baudrate of 115200:
  Serial.begin(115200);
}

void loop() {
  //Testing the PWM ESP32 
    for (int dutyCycle = 0, dutyCycle <=255 ; dutyCycle ++){
        ledoWrite (ledChannel, dutyCycle);
        delayMicroseconds (25);
    }
    for (int dutyCycle = 0, dutyCycle <=255 ; dutyCycle --){
        ledoWrite (ledChannel, dutyCycle);
        delayMicroseconds (25);
    }

  /* Ultrasonic part test
  // Clear the trigPin by setting it LOW:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);

 // Trigger the sensor by setting the trigPin high for 10 microseconds:
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
  duration = pulseIn(echoPin, HIGH);
  
  // Calculate the distance:
  distance = duration*0.034/2; 
  */
}
